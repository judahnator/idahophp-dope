<?php

namespace judahnator\Dope;


final class Math
{

    CONST ADD = '+';
    CONST DIV = '/';
    CONST EXP = '**';
    CONST MUL = '*';
    CONST SUB = '-';

    private $result;

    public function __construct(float &$result = 0)
    {
        $this->result =& $result;
    }

    public function add(float $toAdd): self
    {
        $this->result += $toAdd;
        return $this;
    }

    public function divide(float $toDiv): self
    {
        $this->result /= $toDiv;
        return $this;
    }

    public function exponent(float $toExp = null): self
    {
        $this->result **= $toExp;
        return $this;
    }

    public function getResult(): float
    {
        return $this->result;
    }

    public function parenthesis(string $operator, self $expression): self
    {
        switch ($operator) {
            case static::ADD:
                $method = 'add';
                break;
            case static::DIV:
                $method = 'divide';
                break;
            case static::EXP:
                $method = 'exponent';
                break;
            case static::MUL:
                $method = 'multiply';
                break;
            case static::SUB:
                $method = 'subtract';
                break;
            default:
                throw new \InvalidArgumentException('Unknown operator provided');
        }
        return $this->{$method}($expression->getResult());
    }

    public function multiply(float $toMul): self
    {
        $this->result *= $toMul;
        return $this;
    }

    public function subtract(float $toSub): self
    {
        $this->result -= $toSub;
        return $this;
    }

}