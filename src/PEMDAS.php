<?php

namespace judahnator\Dope;


final class PEMDAS
{

    private $expression = '';
    private $result;

    public function __construct(float &$result = 0)
    {
        $this->result =& $result;
        $this->expression .= (string)$result;
    }

    public function __destruct()
    {
        $this->result = eval("return {$this->expression};");
    }

    public function __toString(): string
    {
        return $this->expression;
    }

    public function add(float $toAdd = null): self
    {
        $this->expression .= "+{$toAdd}";
        return $this;
    }

    public function divide(float $toDiv = null): self
    {
        $this->expression .= "/{$toDiv}";
        return $this;
    }

    public function exponent(float $toExp = null): self
    {
        $this->expression .= "**{$toExp}";
        return $this;
    }

    public function parenthesis(self $expression): self
    {
        $this->expression .= "({$expression})";
        return $this;
    }

    public function multiply(float $toMul = null): self
    {
        $this->expression .= "*{$toMul}";
        return $this;
    }

    public function subtract(float $toSub = null): self
    {
        $this->expression .= "-{$toSub}";
        return $this;
    }

}