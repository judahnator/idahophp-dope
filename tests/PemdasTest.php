<?php

namespace judahnator\Dope\Tests;


use judahnator\Dope\PEMDAS;
use PHPUnit\Framework\TestCase;

final class PemdasTest extends TestCase
{

    public function testPendas(): void
    {
        // 2^3+(4-2)^2/4
        $result = 0;
        (new PEMDAS($result))
            ->add(2)
            ->exponent(3)
            ->add()
            ->parenthesis(
                (new PEMDAS())
                    ->add(4)
                    ->subtract(2)
            )
            ->exponent(2)
            ->divide(4);
        $this->assertEquals(9, $result);
    }

}