<?php

namespace judahnator\Dope\Tests;


use judahnator\Dope\Math;
use PHPUnit\Framework\TestCase;

final class MathTest extends TestCase
{

    public function testMathClass(): void
    {
        // 2^3+(4-2)^2/4
        $result = 0;
        (new Math($result))
            ->add(2)
            ->exponent(3)
            ->parenthesis(
                Math::ADD,
                (new Math())
                    ->add(4)
                    ->subtract(2)
            )
            ->exponent(2)
            ->divide(4);
        $this->assertEquals(9, $result);

    }

}